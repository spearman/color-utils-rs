//! Color constants

//
// monochrome
//
pub const BLACK             : [u8; 4] = [  0,   0,   0, 255];
pub const GREY              : [u8; 4] = [127, 127, 127, 255];
pub const WHITE             : [u8; 4] = [255, 255, 255, 255];

//
// hues
//
// primary
pub const RED               : [u8; 4] = [255,   0,   0, 255];
pub const GREEN             : [u8; 4] = [  0, 255,   0, 255];
pub const BLUE              : [u8; 4] = [  0,   0, 255, 255];
// secondary
pub const CYAN              : [u8; 4] = [  0, 255, 255, 255];
pub const MAGENTA           : [u8; 4] = [255,   0, 255, 255];
pub const YELLOW            : [u8; 4] = [255, 255,   0, 255];
// tertiary
pub const ORANGE            : [u8; 4] = [255, 127,   0, 255];
pub const ROSE              : [u8; 4] = [255,   0, 127, 255];
pub const CHARTREUSE        : [u8; 4] = [127, 255,   0, 255];
pub const VIRIDIAN          : [u8; 4] = [  0, 255, 127, 255];
pub const VIOLET            : [u8; 4] = [127,   0, 255, 255];
pub const AZURE             : [u8; 4] = [  0, 127, 255, 255];

//
// dark
//
// primary
pub const MAROON            : [u8; 4] = [127,   0,   0, 255];
pub const GRASS             : [u8; 4] = [  0, 127,   0, 255];
pub const NAVY              : [u8; 4] = [  0,   0, 127, 255];
// secondary
pub const TEAL              : [u8; 4] = [  0, 127, 127, 255];
pub const PURPLE            : [u8; 4] = [127,   0, 127, 255];
pub const OLIVE             : [u8; 4] = [127, 127,   0, 255];
// tertiary
pub const BROWN             : [u8; 4] = [127,  63,   0, 255];
pub const TYRIAN            : [u8; 4] = [127,   0,  63, 255];
pub const NAPIER            : [u8; 4] = [ 63, 127,   0, 255];
pub const SHAMROCK          : [u8; 4] = [  0, 127,  63, 255];
pub const INDIGO            : [u8; 4] = [ 63,   0, 127, 255];
pub const CERULEAN          : [u8; 4] = [  0,  63, 127, 255];

//
// light
//
pub const PINK              : [u8; 4] = [255, 127, 127, 255];

//
// misc
//
pub const TRANSPARENT       : [u8; 4] = [  0,   0,   0,   0];
pub const TAN               : [u8; 4] = [255, 191, 127, 255];
pub const BEIGE             : [u8; 4] = [255, 255, 191, 255];
// source: <https://en.wikipedia.org/wiki/Blood_red>
pub const BLOOD_RED         : [u8; 4] = [175,  17,  28, 255];
// debug colors
pub const DEBUG_RED         : [u8; 4] = [255,   7,  15, 255];
pub const DEBUG_GREEN       : [u8; 4] = [  0, 255,  31, 255];
pub const DEBUG_BLUE        : [u8; 4] = [ 31,  63, 255, 255];
pub const DEBUG_PINK        : [u8; 4] = [255, 127, 127, 255];
pub const DEBUG_VIOLET      : [u8; 4] = [ 63,   0, 255, 255];
pub const DEBUG_YELLOW      : [u8; 4] = [255, 255,  63, 255];
pub const DEBUG_GOLD        : [u8; 4] = [255, 191,   0, 255];
pub const DEBUG_LIGHT_GREEN : [u8; 4] = [228, 255, 201, 255];
pub const DEBUG_LIGHT_BLUE  : [u8; 4] = [127, 191, 255, 255];
pub const DEBUG_CHARTREUSE  : [u8; 4] = [127, 255,   0, 255];
pub const DEBUG_AZURE       : [u8; 4] = [  0, 127, 255, 255];
pub const DEBUG_GREY        : [u8; 4] = [129, 126, 126, 255];
