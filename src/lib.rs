//! Color utilities.

#![feature(decl_macro)]

extern crate derive_more;

pub mod constants;
pub mod color;

pub use self::constants::*;
pub use self::color::*;

pub fn rgba_u8_to_rgba_f32 (rgba8 : [u8; 4]) -> [f32; 4] {
  [ rgba8[0] as f32 / 255.0,
    rgba8[1] as f32 / 255.0,
    rgba8[2] as f32 / 255.0,
    rgba8[3] as f32 / 255.0 ]
}

pub fn report_sizes() {
  use std::mem::size_of;
  macro_rules! show {
    ($e:expr) => { println!("{}: {:?}", stringify!($e), $e); }
  }
  println!("report sizes...");
  show!(size_of::<Color>());
  println!("...report sizes");
}
