extern crate color_utils;

fn main() {
  println!("color utils: example main...");
  color_utils::report_sizes();
  println!("color utils: ...example main");
}
